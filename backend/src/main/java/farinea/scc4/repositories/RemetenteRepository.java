package farinea.scc4.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import farinea.scc4.domain.Remetente;

@Repository
public interface RemetenteRepository extends JpaRepository<Remetente, String> {
	@Transactional(readOnly=true)
	Remetente findByCpf(String cpf);
}
