package farinea.scc4.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import farinea.scc4.domain.Destinatario;

@Repository
public interface DestinatarioRepository extends JpaRepository<Destinatario, String> {
	@Transactional(readOnly=true)
	Destinatario findByCpf(String cpf);
}
