package farinea.scc4;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Scc4Application implements CommandLineRunner {

	public static void main(String[] args) {
		SpringApplication.run(Scc4Application.class, args);
		
	}

	@Override
	public void run(String... args) throws Exception {
	}

}
