package farinea.scc4.resources;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import farinea.scc4.services.FuncaoNumericaService;

@RestController
@RequestMapping(value="/")
public class FuncaoNumericaResource {
	
	@Autowired
	private FuncaoNumericaService service;
	
	@RequestMapping(value="/listaReversa", params= {"lista"}, method=RequestMethod.GET)
	public ResponseEntity<String> listaReversa(@RequestParam String lista) {
		String listaReversa = service.listaReversa(lista);
		return ResponseEntity.ok().body(listaReversa);
	}

	@RequestMapping(value="/imprimirImpares", params= {"lista"}, method=RequestMethod.GET)
	public ResponseEntity<String> imprimirImpares(@RequestParam String lista) {
		String impares = service.imprimirImpares(lista);
		return ResponseEntity.ok().body(impares);
	}

	@RequestMapping(value="/imprimirPares", params= {"lista"}, method=RequestMethod.GET)
	public ResponseEntity<String> imprimirPares(@RequestParam String lista) {
		String pares = service.imprimirPares(lista);
		return ResponseEntity.ok().body(pares);
	}
}
