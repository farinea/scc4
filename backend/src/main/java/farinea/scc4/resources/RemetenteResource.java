package farinea.scc4.resources;

import java.net.URI;
import java.util.List;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import farinea.scc4.domain.Remetente;
import farinea.scc4.dto.RemetenteDTO;
import farinea.scc4.services.RemetenteService;

@RestController
@RequestMapping(value="/remetentes")
public class RemetenteResource {
	
	@Autowired
	private RemetenteService service;
		
	@RequestMapping(value="/{cpf}", method=RequestMethod.GET)
	public ResponseEntity<Remetente> findByCpf(@PathVariable String cpf) {
		Remetente obj = service.findByCpf(cpf);
		System.out.println(obj.toString());
				
		return ResponseEntity.ok().body(obj);
	}

	@RequestMapping(method=RequestMethod.POST)
	public ResponseEntity<Void> insert(@Valid @RequestBody RemetenteDTO objDto) {
		Remetente obj = service.fromDTO(objDto);
		obj = service.insert(obj);
		URI uri = ServletUriComponentsBuilder.fromCurrentRequest().path("/{nome}").buildAndExpand(obj.getNome()).toUri();
		
		return ResponseEntity.created(uri).build();
	}
	
	@RequestMapping(value="/{cpf}", method=RequestMethod.PUT)
	public ResponseEntity<Void> update(@Valid @RequestBody RemetenteDTO objDto, @PathVariable String cpf) {
		Remetente obj = service.fromDTO(objDto);
		
		obj = service.update(obj);
		
		return ResponseEntity.noContent().build();
	}

	@RequestMapping(value="/{cpf}", method=RequestMethod.DELETE)
	public ResponseEntity<Void> delete(@PathVariable String cpf) {
		service.delete(cpf);
		return ResponseEntity.noContent().build();
	}
	
	@RequestMapping(method=RequestMethod.GET)
	public ResponseEntity<List<RemetenteDTO>> findAll() {
		List<Remetente> list = service.findAll();
		List<RemetenteDTO> listDto = list.stream().map(obj -> new RemetenteDTO(obj)).collect(Collectors.toList());		
		return ResponseEntity.ok().body(listDto);
	}

}
