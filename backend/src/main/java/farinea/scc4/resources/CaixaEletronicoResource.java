package farinea.scc4.resources;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import farinea.scc4.domain.CaixaEletronico;

@RestController
@RequestMapping(value="/")
public class CaixaEletronicoResource {
	
	@Autowired
	private CaixaEletronico caixaEletronico;
	
	@RequestMapping(value="/caixaeletronico", params= {"valor"}, method=RequestMethod.GET)
	public ResponseEntity<String> listaReversa(@RequestParam Integer valor) {
		String resultado = caixaEletronico.getSaque(valor);
		return ResponseEntity.ok().body(resultado);
	}

}
