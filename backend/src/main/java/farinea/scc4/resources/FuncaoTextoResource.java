package farinea.scc4.resources;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import farinea.scc4.services.FuncaoTextoService;

@RestController
@RequestMapping(value="/")
public class FuncaoTextoResource {
	
	@Autowired
	private FuncaoTextoService service;
	
	@RequestMapping(value="/tamanho", params= "palavra", method=RequestMethod.GET)
	public ResponseEntity<String> tamanho(@RequestParam String palavra) {
		String lengthPalavra = service.tamanho(palavra);
		return ResponseEntity.ok().body(lengthPalavra);
	}

	@RequestMapping(value="/maiusculas", params= "palavra", method=RequestMethod.GET)
	public ResponseEntity<String> maiusculas(@RequestParam String palavra) {
		String upperPalavra = service.maiusculas(palavra);
		return ResponseEntity.ok().body(upperPalavra);
	}

	@RequestMapping(value="/vogais", params= "palavra", method=RequestMethod.GET)
	public ResponseEntity<String> vogais(@RequestParam String palavra) {
		String vowelPalavra = service.vogais(palavra);
		return ResponseEntity.ok().body(vowelPalavra);
	}

	@RequestMapping(value="/consoantes", params= "palavra", method=RequestMethod.GET)
	public ResponseEntity<String> consoantes(@RequestParam String palavra) {
		String consonantPalavra = service.consoantes(palavra);
		return ResponseEntity.ok().body(consonantPalavra);
	}

	@RequestMapping(value="/nomeBibliografico", params= "nome", method=RequestMethod.GET)
	public ResponseEntity<String> nomeBibliografico(@RequestParam String nome) {
		String nomeBibliografico = service.nomeBibliografico(nome);
		return ResponseEntity.ok().body(nomeBibliografico);
	}
}
