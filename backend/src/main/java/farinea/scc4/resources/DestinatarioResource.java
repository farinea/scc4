package farinea.scc4.resources;

import java.net.URI;
import java.util.List;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import farinea.scc4.domain.Destinatario;
import farinea.scc4.dto.DestinatarioDTO;
import farinea.scc4.services.DestinatarioService;

@RestController
@RequestMapping(value="/destinatarios")
public class DestinatarioResource {
	
	@Autowired
	private DestinatarioService service;
		
	@RequestMapping(value="/{cpf}", method=RequestMethod.GET)
	public ResponseEntity<Destinatario> findByCpf(@PathVariable String cpf) {
		Destinatario obj = service.findByCpf(cpf);
				
		return ResponseEntity.ok().body(obj);
	}

	@RequestMapping(method=RequestMethod.POST)
	public ResponseEntity<Void> insert(@Valid @RequestBody DestinatarioDTO objDto) {
		Destinatario obj = service.fromDTO(objDto);
		obj = service.insert(obj);
		URI uri = ServletUriComponentsBuilder.fromCurrentRequest().path("/{nome}").buildAndExpand(obj.getNome()).toUri();
		
		return ResponseEntity.created(uri).build();
	}
	
	@RequestMapping(value="/{cpf}", method=RequestMethod.PUT)
	public ResponseEntity<Void> update(@Valid @RequestBody DestinatarioDTO objDto, @PathVariable String cpf) {
		Destinatario obj = service.fromDTO(objDto);
		
		obj = service.update(obj);
		
		return ResponseEntity.noContent().build();
	}

	@RequestMapping(value="/{cpf}", method=RequestMethod.DELETE)
	public ResponseEntity<Void> delete(@PathVariable String cpf) {
		service.delete(cpf);
		return ResponseEntity.noContent().build();
	}
	
	@RequestMapping(method=RequestMethod.GET)
	public ResponseEntity<List<DestinatarioDTO>> findAll() {
		List<Destinatario> list = service.findAll();
		List<DestinatarioDTO> listDto = list.stream().map(obj -> new DestinatarioDTO(obj)).collect(Collectors.toList());		
		return ResponseEntity.ok().body(listDto);
	}

}
