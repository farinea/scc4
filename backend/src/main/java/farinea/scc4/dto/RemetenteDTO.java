package farinea.scc4.dto;

import java.io.Serializable;

import javax.validation.constraints.NotEmpty;

import org.hibernate.validator.constraints.Length;

import farinea.scc4.domain.Remetente;

public class RemetenteDTO implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@NotEmpty(message="Preenchimento obrigatório")
	@Length(min=14, max=14, message="O tamanho deve ser 14 caracteres")
	private String cpf;
	@NotEmpty(message="Preenchimento obrigatório")
	@Length(min=2, max=80, message="O tamanho deve ser entre 2 e 80 caracteres")
	private String nome;
	private String telefone;
	private String endereco;
	private String email;
	
	public RemetenteDTO() {
		
	}

	public RemetenteDTO(Remetente obj) {
		nome = obj.getNome();
		telefone = obj.getTelefone();
		cpf = obj.getCpf();
		endereco = obj.getEndereco();
		email = obj.getEmail();
	}
	
	public RemetenteDTO(String nome, String telefone, String cpf, String endereco, String email) {
		super();
		this.nome = nome;
		this.telefone = telefone;
		this.cpf = cpf;
		this.endereco = endereco;
		this.email = email;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getTelefone() {
		return telefone;
	}

	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public String getEndereco() {
		return endereco;
	}

	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

}
