package farinea.scc4.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import farinea.scc4.domain.Remetente;
import farinea.scc4.dto.RemetenteDTO;
import farinea.scc4.repositories.RemetenteRepository;

@Service
public class RemetenteService {
	@Autowired
	private RemetenteRepository repo;

	public Remetente findByCpf(String cpf) {
		System.out.println(cpf);
		Remetente obj = repo.findByCpf(cpf);
		System.out.println(obj.toString());
		return obj;
	}

	public Remetente insert(Remetente obj) {
		return repo.save(obj);
	}

	public Remetente update(Remetente obj) {
		Remetente newObj = findByCpf(obj.getCpf());
		updateData(newObj, obj);
		return repo.save(newObj);
	}

	public void delete(String cpf) {
		Remetente obj = findByCpf(cpf);
		try {
			repo.delete(obj);
			
		} catch (Exception e) {

		}
	}

	public List<Remetente> findAll() {
		return repo.findAll();
	}

	public Remetente fromDTO(RemetenteDTO objDto) {
		return new Remetente(objDto.getNome(),objDto.getTelefone(),objDto.getCpf(),objDto.getEndereco(),objDto.getEmail());				
	}
	
	private void updateData(Remetente newObj, Remetente obj) {
		newObj.setNome(obj.getNome());
		newObj.setTelefone(obj.getTelefone());
		newObj.setCpf(obj.getCpf());
		newObj.setEndereco(obj.getEndereco());
		newObj.setEmail(obj.getEmail());
	}

}
