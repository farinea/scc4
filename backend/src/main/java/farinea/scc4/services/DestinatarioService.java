package farinea.scc4.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import farinea.scc4.domain.Destinatario;
import farinea.scc4.dto.DestinatarioDTO;
import farinea.scc4.repositories.DestinatarioRepository;

@Service
public class DestinatarioService {
	@Autowired
	private DestinatarioRepository repo;

	public Destinatario findByCpf(String cpf) {
		System.out.println(cpf);
		Destinatario obj = repo.findByCpf(cpf);
		System.out.println(obj.toString());
		return obj;
	}

	public Destinatario insert(Destinatario obj) {
		return repo.save(obj);
	}

	public Destinatario update(Destinatario obj) {
		Destinatario newObj = findByCpf(obj.getCpf());
		updateData(newObj, obj);
		return repo.save(newObj);
	}

	public void delete(String cpf) {
		Destinatario obj = findByCpf(cpf);
		try {
			repo.delete(obj);
			
		} catch (Exception e) {

		}
	}

	public List<Destinatario> findAll() {
		return repo.findAll();
	}

	public Destinatario fromDTO(DestinatarioDTO objDto) {
		return new Destinatario(objDto.getNome(),objDto.getTelefone(),objDto.getCpf(),objDto.getEndereco(),objDto.getEmail());				
	}
	
	private void updateData(Destinatario newObj, Destinatario obj) {
		newObj.setNome(obj.getNome());
		newObj.setTelefone(obj.getTelefone());
		newObj.setCpf(obj.getCpf());
		newObj.setEndereco(obj.getEndereco());
		newObj.setEmail(obj.getEmail());
	}

}
