package farinea.scc4.services;

import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

@Service
public class FuncaoTextoService {

	public String tamanho(String palavra) {
		return "tamanho=" + palavra.length();
	}
	
	public String maiusculas(String palavra) {
		return palavra.toUpperCase();
	}

	public String vogais(String palavra) {
		String vogais= "aeiou";
        String vowelPalavra = "";
        
        for(int i = 0; i < palavra.length(); i++){
                char c = palavra.charAt(i);
                if (vogais.indexOf(String.valueOf(c)) >=0 ) {
                    vowelPalavra += String.valueOf(c);        		                	
                }
        }

		return vowelPalavra;
	}
	
	public String consoantes(String palavra) {
		String vogais= "aeiou";
        String consonantPalavra = "";
        
        for(int i = 0; i < palavra.length(); i++){
                char c = palavra.charAt(i);
                if (vogais.indexOf(String.valueOf(c)) < 0 ) {
                    consonantPalavra += String.valueOf(c);        		                	
                }
        }

		return consonantPalavra;
	}

	public String nomeBibliografico(String nome) {
		String[] arrayNome = nome.split("%");
		
		String nomeBibliografico = arrayNome[arrayNome.length - 1].toUpperCase() + ',';

        for(int i = 0; i < arrayNome.length - 1; i++){
      		nomeBibliografico += ' ' + StringUtils.capitalize(arrayNome[i]);
        }
		
		return nomeBibliografico;
	}

}
