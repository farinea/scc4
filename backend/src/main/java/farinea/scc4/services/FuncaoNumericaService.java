package farinea.scc4.services;

import java.util.Arrays;
import java.util.Collections;

import org.springframework.stereotype.Service;

@Service
public class FuncaoNumericaService {

	public String listaReversa(String lista) {
		String listaNumeros = lista.substring(1, lista.length() - 1);
		
		String[] arrayLista = listaNumeros.split(",");
				
		Collections.reverse(Arrays.asList(arrayLista));

		String listaReversa = Arrays.toString(arrayLista).substring(1, Arrays.toString(arrayLista).length() - 1);
		
        return '{' + listaReversa  + '}';
	}
	
	public String imprimirImpares(String lista) {
		String listaNumeros = lista.substring(1, lista.length() - 1);
		
		String[] arrayLista = listaNumeros.split(",");
		
		String impares = "";
				
		for (int i = 0; i < arrayLista.length; i++) {
		    if (Integer.parseInt(arrayLista[i]) % 2 != 0) {
		        if (impares == "") {
		        	impares = arrayLista[i];
		        } else {
		        	impares += "," + arrayLista[i];		        	
		        }
		    }
		}
        return listaReversa('{' + impares + '}');
	}

	public String imprimirPares(String lista) {
		String listaNumeros = lista.substring(1, lista.length() - 1);
		
		String[] arrayLista = listaNumeros.split(",");
		
		String pares = "";
				
		for (int i = 0; i < arrayLista.length; i++) {
		    if (Integer.parseInt(arrayLista[i]) % 2 == 0) {
		        if (pares == "") {
		        	pares = arrayLista[i];
		        } else {
		        	pares += "," + arrayLista[i];		        	
		        }
		    }
		}
        return listaReversa('{' + pares + '}');
	}
}
