package farinea.scc4.domain;

public class Fracao {
	private Integer numerador;
	private Integer denominador;
	
	public Fracao() {
		super();
	}

	public Fracao(Integer numerador, Integer denominador) {
		super();
		this.numerador = numerador;
		this.denominador = denominador;
	}

	public Integer getNumerador() {
		return numerador;
	}

	public void setNumerador(Integer numerador) {
		this.numerador = numerador;
	}

	public Integer getDenominador() {
		return denominador;
	}

	public void setDenominador(Integer denominador) {
		this.denominador = denominador;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((denominador == null) ? 0 : denominador.hashCode());
		result = prime * result + ((numerador == null) ? 0 : numerador.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Fracao other = (Fracao) obj;
		if (denominador == null) {
			if (other.denominador != null)
				return false;
		} else if (!denominador.equals(other.denominador))
			return false;
		if (numerador == null) {
			if (other.numerador != null)
				return false;
		} else if (!numerador.equals(other.numerador))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return numerador + "/" + denominador;
	}
	
	
	public Fracao soma(Fracao f1, Fracao f2) {
        final Integer novoDenominador = mmc(f1.getDenominador(), f2.getDenominador());
        final Integer novoNumerador = encontraNovoNumerador(f1, novoDenominador) + encontraNovoNumerador(f2, novoDenominador);
        return simplificar( new Fracao(novoNumerador, novoDenominador) );
	}
	
	public Fracao subtracao(Fracao f1, Fracao f2) {
        final Integer novoDenominador = mmc(f1.getDenominador(), f2.getDenominador());
        final Integer novoNumerador = encontraNovoNumerador(f1, novoDenominador) - encontraNovoNumerador(f2, novoDenominador);
        return simplificar( new Fracao(novoNumerador, novoDenominador) );
	}
	
	public Fracao multiplicacao(Fracao f1, Fracao f2) {
        return simplificar( new Fracao(f1.getNumerador() * f2.getNumerador(), f1.getDenominador() * f2.getDenominador()) );
	}
	
	public Fracao divisao(Fracao f1, Fracao f2) {
        return simplificar( new Fracao(f1.getNumerador() * f2.getDenominador(), f1.getDenominador() * f2.getNumerador()) );
	}
    //Função para simplificar a fração
    public Fracao simplificar(Fracao fracao) {
        Integer mdc = mdc(fracao.getNumerador(), fracao.getDenominador());
        return new Fracao(fracao.getNumerador() / mdc, fracao.getDenominador() / mdc);
    }

    //Função para encontrar o novo Numerador
    private Integer encontraNovoNumerador(Fracao fracao, Integer novoDenominador) {
        return novoDenominador / fracao.getDenominador() * fracao.getNumerador();
    }

    //Função para calcular o Mínimo Múltiplo Comum
    private static Integer mmc(Integer a, Integer b) {
        return a * (b / mdc(a, b));
    }
	    
    //Função para calcular o Máximo Divisor Comum
    private static Integer mdc(Integer a, Integer b) {
        while (b != 0) {
            Integer oldB = b;
            b = a % b;
            a = oldB;
        }
        return a;
    }

}
