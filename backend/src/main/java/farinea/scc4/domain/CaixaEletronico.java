package farinea.scc4.domain;

import java.io.Serializable;

import org.springframework.stereotype.Service;

@Service
public class CaixaEletronico implements Serializable {
	private static final long serialVersionUID = 1L;
	
	//variável possibilita alterar o valor das notas se necessário, sem alterar a função getSaque
	private Integer[] notas = new Integer[] { 3, 5 };

	public CaixaEletronico() {
		super();
	}

	public String getSaque(Integer valor) {
		Integer restoMaior = valor % this.notas[1];
		Integer notaMenor = 0;
		Integer notaMaior = 0;
		String  resultado = "Saque R$" + valor + ": ";
		
		if (restoMaior == 0) {
			notaMaior = valor / this.notas[1];
			resultado += notaMaior + (notaMaior > 1 ? " notas" : " nota") + " de R$" + this.notas[1];
		} else if (restoMaior >= this.notas[0]) {
			if ((restoMaior % this.notas[0]) != 0) {
				resultado = "Valor indisponível para saque";
			} else {
				notaMenor = restoMaior / this.notas[0];
				notaMaior = valor / this.notas[1];	
				resultado += notaMenor + (notaMenor > 1 ? " notas" : " nota") + " de R$" + this.notas[0];
				if (notaMaior > 0) {
					resultado += " e " + notaMaior + (notaMaior > 1 ? " notas" : " nota") + " de R$" + this.notas[1];
				}
			}
		} else  {
			restoMaior = (valor - this.notas[0]) % this.notas[1];
			if ((restoMaior != 0) && ((valor - (( (valor - this.notas[0]) / this.notas[1]) * this.notas[1])) % this.notas[0] != 0)) {
				resultado = "Valor indisponível para saque";				
			} else {	
				notaMaior = (valor - this.notas[0]) / this.notas[1];
				notaMenor = (valor -  (( (valor - this.notas[0]) / this.notas[1]) * this.notas[1])) / this.notas[0];
				resultado += notaMenor + (notaMenor > 1 ? " notas" : " nota") + " de R$" + this.notas[0];
				if (notaMaior > 0) {
					resultado += " e " + notaMaior + (notaMaior > 1 ? " notas" : " nota") + " de R$" + this.notas[1];
				}
			}
		}
		
		return resultado;		
	}
	
}
