import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },
  {
    path: 'home',
    loadChildren: () => import('./home/home.module').then(m => m.HomePageModule)
  },   {
    path: 'remetente',
    loadChildren: () => import('./remetente/remetente.module').then( m => m.RemetentePageModule)
  },
  {
    path: 'remetente-form/:cpf',
    loadChildren: () => import('./remetente-form/remetente-form.module').then( m => m.RemetenteFormPageModule)
  },
  {
    path: 'destinatario-file',
    loadChildren: () => import('./destinatario-file/destinatario-file.module').then( m => m.DestinatarioFilePageModule)
  },
  {
    path: 'destinatario',
    loadChildren: () => import('./destinatario/destinatario.module').then( m => m.DestinatarioPageModule)
  },
  {
    path: 'destinatario-form/:cpf',
    loadChildren: () => import('./destinatario-form/destinatario-form.module').then( m => m.DestinatarioFormPageModule)
  },

];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
