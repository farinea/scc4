import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DestinatarioFormPageRoutingModule } from './destinatario-form-routing.module';

import { DestinatarioFormPage } from './destinatario-form.page';

import {NgxMaskIonicModule} from 'ngx-mask-ionic'


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReactiveFormsModule,
    NgxMaskIonicModule,
    DestinatarioFormPageRoutingModule
  ],
  declarations: [DestinatarioFormPage]
})
export class DestinatarioFormPageModule {}
