import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DestinatarioFormPage } from './destinatario-form.page';

const routes: Routes = [
  {
    path: '',
    component: DestinatarioFormPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DestinatarioFormPageRoutingModule {}
