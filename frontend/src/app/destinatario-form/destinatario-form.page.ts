import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { DestinatarioDTO } from 'src/models/destinatario.dto';
import { ActivatedRoute, Router } from '@angular/router';
import { NavController, AlertController } from '@ionic/angular';
import { DestinatarioService } from 'src/services/domain/destinatario.service';

@Component({
  selector: 'app-destinatario-form',
  templateUrl: './destinatario-form.page.html',
  styleUrls: ['./destinatario-form.page.scss'],
})
export class DestinatarioFormPage implements OnInit {

  formGroup: FormGroup;
  destinatario: DestinatarioDTO;
  cpf: string;

  constructor(
    private activatedRoute: ActivatedRoute,
    public rota: Router,
    public navCtrl: NavController, 
    public formBuilder: FormBuilder,
    public alertCtrl: AlertController,
    public destinatarioService: DestinatarioService) { 

      this.formGroup = this.formBuilder.group({
        cpf : ['', [Validators.required, Validators.minLength(14), Validators.maxLength(14)]],
        nome: ['', [Validators.required, Validators.minLength(2), Validators.maxLength(120)]],
        telefone : [ '', [Validators.required]],
        endereco : ['', [Validators.required]],
        email: ['', [Validators.required, Validators.email]]
      });
    }


  ngOnInit() {
    this.cpf = this.activatedRoute.snapshot.paramMap.get('cpf');
    if (this.cpf != "novo") {
      this.destinatarioService.findByCpf(this.cpf)
      .subscribe(response => {
        this.destinatario = response;
        this.formGroup.setValue({
          cpf: this.destinatario.cpf,
          nome: this.destinatario.nome,
          telefone: this.destinatario.telefone,
          endereco: this.destinatario.endereco,
          email: this.destinatario.email
        });
      },
      error => {}); 
  }

  }

  saveDestinatario() {
    if (this.cpf=="novo") {
      this.destinatarioService.insert(this.formGroup.value)
      .subscribe(response => {
        this.showOk("Inclusão");
      },
      error => {});
    } else {
      this.destinatarioService.update(this.formGroup.value)
      .subscribe(response => {
        this.showOk("Alteração");
      },
      error => {});
    }
  }

  deleteDestinatario() {
    if (this.cpf!="novo") {
      this.destinatarioService.delete(this.formGroup.value)
      .subscribe(response => {
        this.showOk("Exclusão");
      },
      error => {});
    } 
  }


  async showOk(operacao: string) {
    let alert = this.alertCtrl.create({
      header: 'Sucesso!',
      message: operacao + ' efetuada com sucesso',
      backdropDismiss: false,
      buttons: [
        {
          text: 'Ok',
          handler: () => {
            this.navCtrl.pop();
            this.navCtrl.navigateRoot(`/destinatario`)
          }
        }
      ]
    });
    (await alert).present();
  }  
}
