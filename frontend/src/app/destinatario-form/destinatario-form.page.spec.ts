import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { DestinatarioFormPage } from './destinatario-form.page';

describe('DestinatarioFormPage', () => {
  let component: DestinatarioFormPage;
  let fixture: ComponentFixture<DestinatarioFormPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DestinatarioFormPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(DestinatarioFormPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
