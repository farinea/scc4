import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DestinatarioFilePage } from './destinatario-file.page';

const routes: Routes = [
  {
    path: '',
    component: DestinatarioFilePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DestinatarioFilePageRoutingModule {}
