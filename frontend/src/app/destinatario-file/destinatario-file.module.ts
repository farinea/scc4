import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DestinatarioFilePageRoutingModule } from './destinatario-file-routing.module';

import { DestinatarioFilePage } from './destinatario-file.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DestinatarioFilePageRoutingModule
  ],
  declarations: [DestinatarioFilePage]
})
export class DestinatarioFilePageModule {}
