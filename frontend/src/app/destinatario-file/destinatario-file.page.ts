import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { NavController, AlertController } from '@ionic/angular';
import { DestinatarioDTO } from 'src/models/destinatario.dto';
import { ViewChild } from '@angular/core';
import { DestinatarioService } from 'src/services/domain/destinatario.service';

@Component({
  selector: 'app-destinatario-file',
  templateUrl: './destinatario-file.page.html',
  styleUrls: ['./destinatario-file.page.scss'],
})
export class DestinatarioFilePage implements OnInit {

  formGroup: FormGroup;
  destinatario: DestinatarioDTO[] = [];

  public csvRecords: any[] = [];

  @ViewChild('fileImportInput',null) fileImportInput: any;

  constructor(
    public navCtrl: NavController, 
    public formBuilder: FormBuilder,
    public alertCtrl: AlertController,
    public destinatarioService: DestinatarioService) { 

    }

  ngOnInit() {
  }

  fileChangeListener($event: any): void {
    let text = [];
    let files = $event.srcElement.files;

    if (this.isCSVFile(files[0])) {

      let input = $event.target;
      let reader = new FileReader();
      reader.readAsText(input.files[0]);

      reader.onload = () => {
        let csvData = reader.result;
        let csvRecordsArray = (<string>csvData).split(/\r\n|\n/);

        let headersRow = this.getHeaderArray(csvRecordsArray);

        this.getDataRecordsArrayFromCSVFile(csvRecordsArray, headersRow.length);
      };

      reader.onerror = function () {
        alert('Não foi possível ler o arquivo ' + input.files[0]);
      };

    } else {
      alert("Extensão de Arquivo não Reconhecida. Extensão esperada = .csv)");
      this.fileImportInput.nativeElement.value = "";
    }
  }

  getDataRecordsArrayFromCSVFile(csvRecordsArray: any, headerLength: any) {    
    for (let i = 1; i < csvRecordsArray.length; i++) {
      let data = (<string>csvRecordsArray[i]).split(',');

      // Faz a leitura do arquivo .csv caso o número de colunas seja igual a do cabeçalho
      if (data.length == headerLength) {

        this.destinatario.push({
          cpf:data[0].trim(),
          nome:data[1].trim(),
          telefone:data[2].trim(),
          endereco:data[3].trim(),
          email:data[4].trim()
        });
      }
    }
  }

  // verifica se a extensão do arquivo é .csv
  isCSVFile(file: any) {
    return file.name.endsWith(".csv");
  }

  // retorna o cabeçalho do arquivo
  getHeaderArray(csvRecordsArray: any) {
    let headers = (<string>csvRecordsArray[0]).split(',');
    let headerArray = [];
    for (let j = 0; j < headers.length; j++) {
      headerArray.push(headers[j]);
    }
    return headerArray;
  }

  insertDestinatario() {
    for (let i = 0; i < this.destinatario.length; i++) {
      this.destinatarioService.insert(this.destinatario[i])
      .subscribe(response => {
      },
      error => {});
    }
  }
}
