import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { DestinatarioFilePage } from './destinatario-file.page';

describe('DestinatarioFilePage', () => {
  let component: DestinatarioFilePage;
  let fixture: ComponentFixture<DestinatarioFilePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DestinatarioFilePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(DestinatarioFilePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
