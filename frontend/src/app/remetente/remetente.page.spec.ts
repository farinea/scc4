import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { RemetentePage } from './remetente.page';

describe('RemetentePage', () => {
  let component: RemetentePage;
  let fixture: ComponentFixture<RemetentePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RemetentePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(RemetentePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
