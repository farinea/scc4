import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { RemetentePageRoutingModule } from './remetente-routing.module';

import { RemetentePage } from './remetente.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReactiveFormsModule,
    RemetentePageRoutingModule
  ],
  declarations: [RemetentePage]
})
export class RemetentePageModule {}
