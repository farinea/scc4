import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RemetentePage } from './remetente.page';

const routes: Routes = [
  {
    path: '',
    component: RemetentePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RemetentePageRoutingModule {}
