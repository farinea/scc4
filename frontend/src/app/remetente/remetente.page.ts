import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { NavController, AlertController } from '@ionic/angular';
import { RemetenteService } from 'src/services/domain/remetente.service';
import { RemetenteDTO } from 'src/models/remetente.dto';

@Component({
  selector: 'app-remetente',
  templateUrl: './remetente.page.html',
  styleUrls: ['./remetente.page.scss'],
})
export class RemetentePage implements OnInit {
  
  formGroup: FormGroup;
  remetente: RemetenteDTO[];

  constructor(
    public navCtrl: NavController, 
    public formBuilder: FormBuilder,
    public alertCtrl: AlertController,
    public remetenteService: RemetenteService) { 

    }


  ngOnInit() {
        //busca todos os remetentes
        this.remetenteService.findAll()
        .subscribe(response => {
          this.remetente = response;
        },
        error => {});     
  }

}
