import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { RemetenteFormPage } from './remetente-form.page';

describe('RemetenteFormPage', () => {
  let component: RemetenteFormPage;
  let fixture: ComponentFixture<RemetenteFormPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RemetenteFormPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(RemetenteFormPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
