import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RemetenteFormPage } from './remetente-form.page';

const routes: Routes = [
  {
    path: '',
    component: RemetenteFormPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RemetenteFormPageRoutingModule {}
