import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { RemetenteFormPageRoutingModule } from './remetente-form-routing.module';

import { RemetenteFormPage } from './remetente-form.page';
import {NgxMaskIonicModule} from 'ngx-mask-ionic'

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReactiveFormsModule,
    NgxMaskIonicModule,
    RemetenteFormPageRoutingModule
  ],
  declarations: [RemetenteFormPage]
})
export class RemetenteFormPageModule {}
