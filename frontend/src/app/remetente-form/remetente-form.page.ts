import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { NavController, AlertController } from '@ionic/angular';
import { RemetenteService } from 'src/services/domain/remetente.service';
import { RemetenteDTO } from 'src/models/remetente.dto';

@Component({
  selector: 'app-remetente-form',
  templateUrl: './remetente-form.page.html',
  styleUrls: ['./remetente-form.page.scss'],
})
export class RemetenteFormPage implements OnInit {

  formGroup: FormGroup;
  remetente: RemetenteDTO;
  cpf: string;

  constructor(
    private activatedRoute: ActivatedRoute,
    public rota: Router,
    public navCtrl: NavController, 
    public formBuilder: FormBuilder,
    public alertCtrl: AlertController,
    public remetenteService: RemetenteService) { 

      this.formGroup = this.formBuilder.group({
        cpf : ['', [Validators.required, Validators.minLength(14), Validators.maxLength(14)]],
        nome: ['', [Validators.required, Validators.minLength(2), Validators.maxLength(120)]],
        telefone : [ '', [Validators.required]],
        endereco : ['', [Validators.required]],
        email: ['', [Validators.required, Validators.email]]
      });
    }


  ngOnInit() {
    this.cpf = this.activatedRoute.snapshot.paramMap.get('cpf');
    if (this.cpf != "novo") {
      console.log(this.cpf);
      this.remetenteService.findByCpf(this.cpf)
      .subscribe(response => {
        this.remetente = response;
        console.log(response);
        console.log(this.remetente);
        this.formGroup.setValue({
          cpf: this.remetente.cpf,
          nome: this.remetente.nome,
          telefone: this.remetente.telefone,
          endereco: this.remetente.endereco,
          email: this.remetente.email
        });
        console.log(this.formGroup.value)
      },
      error => {}); 
  }

  }

  saveRemetente() {
    if (this.cpf=="novo") {
      this.remetenteService.insert(this.formGroup.value)
      .subscribe(response => {
        this.showOk("Inclusão");
      },
      error => {});
    } else {
      this.remetenteService.update(this.formGroup.value)
      .subscribe(response => {
        this.showOk("Alteração");
      },
      error => {});
    }
  }

  deleteRemetente() {
    if (this.cpf!="novo") {
      this.remetenteService.delete(this.formGroup.value)
      .subscribe(response => {
        this.showOk("Exclusão");
      },
      error => {});
    } 
  }


  async showOk(operacao: string) {
    let alert = this.alertCtrl.create({
      header: 'Sucesso!',
      message: operacao + ' efetuada com sucesso',
      backdropDismiss: false,
      buttons: [
        {
          text: 'Ok',
          handler: () => {
            this.navCtrl.pop();
            this.navCtrl.navigateRoot(`/remetente`)
          }
        }
      ]
    });
    (await alert).present();
  }  
}
