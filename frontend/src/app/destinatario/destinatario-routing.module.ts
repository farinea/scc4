import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DestinatarioPage } from './destinatario.page';

const routes: Routes = [
  {
    path: '',
    component: DestinatarioPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DestinatarioPageRoutingModule {}
