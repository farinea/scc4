import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { NavController, AlertController } from '@ionic/angular';
import { DestinatarioDTO } from 'src/models/destinatario.dto';
import { DestinatarioService } from 'src/services/domain/destinatario.service';


@Component({
  selector: 'app-destinatario',
  templateUrl: './destinatario.page.html',
  styleUrls: ['./destinatario.page.scss'],
})

export class DestinatarioPage implements OnInit {

  formGroup: FormGroup;
  destinatario: DestinatarioDTO[];

  constructor(
    public navCtrl: NavController, 
    public formBuilder: FormBuilder,
    public alertCtrl: AlertController,
    public destinatarioService: DestinatarioService) { 

    }


  ngOnInit() {
        //busca todos os destinatarios
        this.destinatarioService.findAll()
        .subscribe(response => {
          this.destinatario = response;
        },
        error => {});   
  }

}