import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DestinatarioPageRoutingModule } from './destinatario-routing.module';

import { DestinatarioPage } from './destinatario.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DestinatarioPageRoutingModule
  ],
  declarations: [DestinatarioPage]
})
export class DestinatarioPageModule {}
