import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { DestinatarioPage } from './destinatario.page';

describe('DestinatarioPage', () => {
  let component: DestinatarioPage;
  let fixture: ComponentFixture<DestinatarioPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DestinatarioPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(DestinatarioPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
