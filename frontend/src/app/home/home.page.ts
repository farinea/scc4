import { Component, OnInit } from '@angular/core';
import { RemetenteDTO } from 'src/models/remetente.dto';
import { DestinatarioDTO } from 'src/models/destinatario.dto';
import { DestinatarioService } from 'src/services/domain/destinatario.service';
import { RemetenteService } from 'src/services/domain/remetente.service';
import { NavController, AlertController } from '@ionic/angular';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {

  remetente: RemetenteDTO[];
  remetenteSelect: string = null;

  destinatario: DestinatarioDTO[];
  destinatarioSelect: string[] = null;

  remetenteNome : string = '';
  destinatarioNome : string = '';

  constructor(
    public remetenteService: RemetenteService,
    public destinatarioService: DestinatarioService,
    public navCtrl: NavController,
    public alertCtrl: AlertController
  ) {


  }

  ngOnInit() {
    //busca todos os remetentes
    this.remetenteService.findAll()
        .subscribe(response => {
          this.remetente = response;
        },
        error => {});   
    
    //busca todos os destinatarios
    this.destinatarioService.findAll()
        .subscribe(response => {
          this.destinatario = response;
    },
    error => {});   
}

  remetenteSelected() {
    if (this.remetenteSelect != null) {
      this.remetenteService.findByCpf(this.remetenteSelect)
      .subscribe(response => {
        this.remetenteNome = response.nome; 
      },
      error => {}); 
  
    }
  }

  destinatarioSelected() {
    if (this.destinatarioSelect != null) {
      this.destinatarioNome = '';
      for (let i = 0; i < this.destinatarioSelect.length; i++) {
        this.destinatarioService.findByCpf(this.destinatarioSelect[i])
          .subscribe(response => {
            this.destinatarioNome += response.nome;

            if (i<this.destinatarioSelect.length-2) {
              this.destinatarioNome += ', ' 
            } if (i==this.destinatarioSelect.length-2) {
              this.destinatarioNome += ' e ' 
            }
          },
          error => {}); 
      }
    }
  }

  sendMessage() {
    let mensagem: string = this.remetenteNome + " enviou uma mensagem para " + this.destinatarioNome;
    console.log(mensagem);  
    this.showMessage(mensagem);
  }

  async showMessage(mensagem: string) {
    let alert = this.alertCtrl.create({
      header: 'Mensagem',
      message: mensagem,
      backdropDismiss: false,
      buttons: [
        {
          text: 'Ok',
          handler: () => {
            this.navCtrl.pop();
            this.remetenteSelect = null;
            this.destinatarioSelect = [];
          }
        }
      ]
    });
    (await alert).present();
  }  

}
