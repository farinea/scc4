import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Observable, throwError } from "rxjs";
import { API_CONFIG } from "../../config/api.config";
import { DestinatarioDTO } from 'src/models/Destinatario.dto';
import { catchError, map } from 'rxjs/operators';

@Injectable()
export class DestinatarioService {

    constructor(public http: HttpClient) {
    }

    httpOptions = {
        headers: new HttpHeaders({
          'Content-Type':  'application/json'
        })
    };

    findAll() : Observable<DestinatarioDTO[]> {
        return <Observable<DestinatarioDTO[]>> this.http.get(`${API_CONFIG.baseUrl}/destinatarios`);
    }

    findByCpf(cpf: string) : Observable<DestinatarioDTO>{
        return <Observable<DestinatarioDTO>> this.http.get(`${API_CONFIG.baseUrl}/destinatarios/${cpf}`);
    }

    insert(obj : DestinatarioDTO) {
        return this.http.post(
            `${API_CONFIG.baseUrl}/destinatarios`, 
            obj,this.httpOptions).pipe(
                catchError(this.handleError),
                map(() => obj)
              );
    }

    update(obj : DestinatarioDTO) {
        return this.http.put(
            `${API_CONFIG.baseUrl}/destinatarios/${obj.cpf}`, 
            obj,this.httpOptions).pipe(
                catchError(this.handleError),
                map(() => obj)
              );
    }

    delete(obj : DestinatarioDTO) {
        console.log('passou');
        return this.http.delete(
            `${API_CONFIG.baseUrl}/destinatarios/${obj.cpf}`, 
                this.httpOptions).pipe(
                catchError(this.handleError),
                map(() => obj)
              );
    }

    private handleError(error: any): Observable<any> {

        console.log("Erro na requisição =>", error);
        return throwError(error);
      }

} 