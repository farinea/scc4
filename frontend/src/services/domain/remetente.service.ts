import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Observable, throwError } from "rxjs";
import { API_CONFIG } from "../../config/api.config";
import { RemetenteDTO } from 'src/models/remetente.dto';
import { catchError, map } from 'rxjs/operators';

@Injectable()
export class RemetenteService {

    constructor(public http: HttpClient) {
    }

    httpOptions = {
        headers: new HttpHeaders({
          'Content-Type':  'application/json'
        })
    };

    findAll() : Observable<RemetenteDTO[]> {
        return <Observable<RemetenteDTO[]>> this.http.get(`${API_CONFIG.baseUrl}/remetentes`);
    }

    findByCpf(cpf: string) : Observable<RemetenteDTO>{
        return <Observable<RemetenteDTO>> this.http.get(`${API_CONFIG.baseUrl}/remetentes/${cpf}`);
    }

    insert(obj : RemetenteDTO) {
        return this.http.post(
            `${API_CONFIG.baseUrl}/remetentes`, 
            obj,this.httpOptions).pipe(
                catchError(this.handleError),
                map(() => obj)
              );
    }

    update(obj : RemetenteDTO) {
        return this.http.put(
            `${API_CONFIG.baseUrl}/remetentes/${obj.cpf}`, 
            obj,this.httpOptions).pipe(
                catchError(this.handleError),
                map(() => obj)
              );
    }

    delete(obj : RemetenteDTO) {
        console.log('passou');
        return this.http.delete(
            `${API_CONFIG.baseUrl}/remetentes/${obj.cpf}`, 
                this.httpOptions).pipe(
                catchError(this.handleError),
                map(() => obj)
              );
    }

    private handleError(error: any): Observable<any> {

        console.log("Erro na requisição =>", error);
        return throwError(error);
      }

} 