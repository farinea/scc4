export interface RemetenteDTO {
    cpf : string;
    nome : string;
    telefone : string;
    endereco : string;
    email : string;
}