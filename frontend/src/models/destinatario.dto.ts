export interface DestinatarioDTO {
    cpf : string;
    nome : string;
    telefone : string;
    endereco : string;
    email : string;
}